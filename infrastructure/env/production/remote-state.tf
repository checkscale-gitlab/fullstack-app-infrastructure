terraform {
  backend "s3" {
    bucket         = "klingon-terraform-state"
    encrypt        = true
    dynamodb_table = "klingon-terraform-state"
    key            = "terraform.tfstate"
    region         = "us-east-1"
  }
}
