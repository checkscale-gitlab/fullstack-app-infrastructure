#!/usr/bin/env bash
sudo apt-get install -y apt-transport-https
sudo apt-get install -y software-properties-common wget
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
sudo apt-get update
sudo apt-get install grafana -y

sudo apt-get install jq -y
git clone https://github.com/monitoringartist/grafana-aws-cloudwatch-dashboards
cd rafana-aws-cloudwatch-dashboards
touch import.sh

sudo systemctl daemon-reload
sudo systemctl start grafana-server
sudo systemctl status grafana-server
sudo systemctl enable grafana-server.service